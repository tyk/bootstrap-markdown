Bootstrap-Markdown
------

本项目是一个基于Bootstrap开发的一个Markdown编辑器。本项目是在[https://github.com/michaelliao/bootstrap.git](https://github.com/michaelliao/bootstrap.git)基础上的改进版，旧项目支持Bootstrap2.x，新版支持Bootstrap3.x
  
使用示例
------
	$(function() {
	    $('#markdown-editor').markdown();
	});

	<form role="form">
	    <div class="form-group">
	        <label for="Content">Email address</label>
	        <textarea id="markdown-editor" class="form-control" rows="12"></textarea>
	    </div>
	</form>

定制
------
可以修改`markdown-editor-N`文件进行私有化定制，在新版中有详尽的注释。项目本身极易扩展。  

Demo
------
leadership.html和leadership-N.html分别对应旧版和新版编辑器示例